package cn.edu.cnu.training.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.edu.cnu.training.dao.BaseDAO;
import cn.edu.cnu.training.pojo.Answer;
import net.sf.json.JSONObject;

public class TestProblem {

	static ClassPathXmlApplicationContext classPathXmlApplicationContext;
	static BaseDAO<Answer> answerBaseDAO;
	
	@BeforeClass
	public static void beforeClass(){
		classPathXmlApplicationContext = new ClassPathXmlApplicationContext("applicationContext.xml","hibernateContext.xml");
		answerBaseDAO = classPathXmlApplicationContext.getBean(BaseDAO.class);
	}
	
	@Test
	public void test() {
		//hql语言中lower()方法将所有的字符自动转换成小写再参与查询。这样就可以采用英文忽略大小写的方式查询了
		List<Answer> answers = answerBaseDAO.list("from Answer as a where lower(a.content) like lower('%" + "每天" +  "%')");
		System.out.println(answers.size());
		for(Answer answer : answers){
			System.out.println(JSONObject.fromObject(answer).toString());
		}
	}

}
