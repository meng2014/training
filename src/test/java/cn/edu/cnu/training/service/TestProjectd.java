package cn.edu.cnu.training.service;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.edu.cnu.training.dao.BaseDAO;
import cn.edu.cnu.training.pojo.Project;
import cn.edu.cnu.training.pojo.ProjectNode;
import net.sf.json.JSONObject;

public class TestProjectd {

	static ClassPathXmlApplicationContext applicationContextAware = null;
	static BaseDAO<ProjectNode> projectNodeBaseDAO = null;
	@BeforeClass
	public static void BeforeClass(){
		applicationContextAware = new ClassPathXmlApplicationContext("applicationContext.xml","hibernateContext.xml");
		projectNodeBaseDAO = applicationContextAware.getBean(BaseDAO.class);
	}
	
	@Test
	public void test() {
		List<ProjectNode> nodes = projectNodeBaseDAO.list("from ProjectNode as pn where pn.project.projectId = " + Integer.valueOf("1"));
		System.out.println("------------------------------------------");
		System.out.println("nodes : " + nodes.size());
		for(ProjectNode projectNode : nodes){
			System.out.println(JSONObject.fromObject(projectNode).toString());
		}
		
	}
	
	@Test
	public void testDelete(){
		
	}

}
