package cn.edu.cnu.training.service;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.edu.cnu.training.dao.BaseDAO;
import cn.edu.cnu.training.pojo.User;

public class TestUserService {
	
	static ClassPathXmlApplicationContext applicationContextAware;
	static BaseDAO<User> userDAO ;
	
	@BeforeClass
	public static void BeforeClass(){
		applicationContextAware = new ClassPathXmlApplicationContext("applicationContext.xml","hibernateContext.xml");
		userDAO = (BaseDAO<User>)applicationContextAware.getBean(BaseDAO.class);
	}
	
	
	@Test
	public void TestLogin(){
		
		
		System.out.println(userDAO.lookup(User.class, 1).getName());
	}
	
	/**
	 * 可以只知道id就把记录删除
	 */
	@Test
	public void TestDelete(){
		User user = new User();
		user.setId(5);
		userDAO.delete(user);
	}
	
}
