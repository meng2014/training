package cn.edu.cnu.training.vo;

import java.util.List;

import cn.edu.cnu.training.pojo.User;

public class Group {
	private String groupId;
	private List<User> members;
	private int headman;
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public List<User> getMembers() {
		return members;
	}
	public void setMembers(List<User> members) {
		this.members = members;
	}
	public int getHeadman() {
		return headman;
	}
	public void setHeadman(int headman) {
		this.headman = headman;
	}
}
