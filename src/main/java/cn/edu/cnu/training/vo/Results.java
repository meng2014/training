package cn.edu.cnu.training.vo;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * 从服务器返回到客户端的返回值结构
 * @author meng
 *
 */
public class Results {
	/**
	 * 返回提示信息
	 */
	private String msg;
	/**
	 * 返回状态信息。1表示正确返回，2表示错误返回。
	 */
	private String status;
	public static String OK = "1";
	public static String FAIL = "2";
	
	/**
	 * 所有的返回值信息都保存到这里
	 */
	private Map<String,Object> datas;
	
	public Results(){
		datas = new HashMap<String,Object>();
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Map<String, Object> getDatas() {
		return datas;
	}

	public void setDatas(Map<String, Object> datas) {
		this.datas = datas;
	}
	
	public void setData(String key,Object data){
		datas.put(key, data);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
