package cn.edu.cnu.training.service;

import static cn.edu.cnu.training.util.ResultHelper.packaging;
import static cn.edu.cnu.training.util.Variables.USER;
import static cn.edu.cnu.training.util.Variables.classId;
import static cn.edu.cnu.training.util.Variables.produces;
import static cn.edu.cnu.training.vo.Results.FAIL;
import static cn.edu.cnu.training.vo.Results.OK;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.edu.cnu.training.dao.BaseDAO;
import cn.edu.cnu.training.exception.ParamException;
import cn.edu.cnu.training.exception.ParamFormateException;
import cn.edu.cnu.training.pojo.Classes;
import cn.edu.cnu.training.pojo.Notice;
import cn.edu.cnu.training.pojo.Problem;
import cn.edu.cnu.training.pojo.Project;
import cn.edu.cnu.training.pojo.ProjectNode;
import cn.edu.cnu.training.pojo.Resource;
import cn.edu.cnu.training.pojo.User;
import cn.edu.cnu.training.util.JsonHelper;
import cn.edu.cnu.training.util.StringHelper;
import cn.edu.cnu.training.util.Variables;
import net.sf.json.JSONObject;


@Component
@Produces(produces)
public class ProjectService {
	
	private static Logger logger = LogManager.getLogger(ProjectService.class);
	@Autowired
	private BaseDAO<Project> projectBaseDAO;
	@Autowired
	private BaseDAO<ProjectNode> projectNodeBaseDAO;
	@Autowired
	private BaseDAO<Resource> resourceNodeBaseDAO;
	@Autowired
	private BaseDAO<Notice> noticeBaseDAO;
	@Autowired
	private BaseDAO<Problem> problemBaseDAO;
	
	
	/**
	 * 添加项目属性，并没有添加项目节点
	 * 首先添加项目属性到数据库，然后开始添加节点，首先上传文件到本地，返回路径，然后上传该节点的其他信息，保存到session中。
	 	直到用户点击"完成"才开始从session中获取并保存到数据库中
	 * 页面上的字段名字需要按照实体类中名字命名
	 * {
	 * 	"classId":"1",
	 * 	"title":"aaa"
	 * }
	 * @param json
	 * @param request
	 * @return
	 */
	@POST
	@Path("/add")
	public Response addProjectParam(String json,@Context HttpServletRequest request){
		
		User login = (User)request.getAttribute(USER);
		//只能教师上传项目
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		logger.info("project : " + json);
		Project project = JsonHelper.jsonToBean(json,Project.class);
		
		if (project == null) {
			return packaging(FAIL, "数据格式错误，请核对后从新上传!");
		}
		
		//检验必要的数据:项目起止时间
		if(project.getStartTime() == null || project.getStartTime().equals(null) || project.getEndTime() == null || project.getEndTime().equals(null)){
			return packaging(FAIL, "项目起止时间格式不对或没有指定！");
		}
		if (!JSONObject.fromObject(json).containsKey(classId)) {
			return packaging(FAIL, "班级Id参数出错！");
		}
		int classId = 0;
		try{
			classId = JSONObject.fromObject(json).getInt(Variables.classId);
		}catch(Throwable throwable){
			logger.error(throwable, throwable);
			return packaging(FAIL, "班级Id参数出错！");
		}
		
		//数据初试化
		project.setCreateTime(new Date());
		project.setStatus(false);
		project.setPublishTime(null);
		project.setCreator(login);
		if (project.getTitle() == null || project.getTitle().equals(null)) {
			project.setTitle(login.getName() + "创建的项目");
		}
		
		Classes classes = new Classes();
		classes.setClassId(classId);
		project.setClasses(classes);
		
		
		//保存到数据库
		Project project2 = projectBaseDAO.create(project);
		return (project2 == null)?packaging(FAIL, "项目添加失败，请重试！"):packaging(OK, "project", project2);
	}
	
	/**
	 * 添加新项目流程时，下一步之后提交到服务器，如果有文件，则保存文件到本地，一切数据暂时保存在session中。等用户点击完成之后，一起添加到数据库
	 * 将一个node的属性和resource的属性全部提交到服务器。另外还需要项目id，节点在项目中先后序号nodeNum(从0开始编号)
	 * {
	 * 	"projectId":"14",
	 * 	"nodeNum":"2"
	 * 	"locatioin":""
	 * }
	 * @return
	 */
	@POST
	@Path("/node/add")
	public Response addProjectNode(String json,@Context HttpServletRequest request){
		logger.info("addProjectNode.json : " + json);
		User login = (User)request.getAttribute(USER);
		//只能教师上传项目
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		//获取相应的数据并装填成相应的对象
		ProjectNode projectNode = JsonHelper.jsonToBean(json, ProjectNode.class);
		Resource resource = JsonHelper.jsonToBean(json, Resource.class);
		if (projectNode == null || resource == null) {
			return packaging(FAIL, "参数不对！");
		}
		
		
		int projectId = 0;
		int nodeNum = 0;
		Date startTime = null;
		Date endTime = null;
		try {
			projectId = JsonHelper.getParam(json,Variables.projectId);
			nodeNum = JsonHelper.getParam(json,Variables.nodeNum);
			startTime = JsonHelper.getDateParam(json, Variables.startTime);
			endTime = JsonHelper.getDateParam(json, Variables.endTime);
			
		} catch (ParamFormateException e) {
			logger.error(e, e);
			return packaging(FAIL, "参数格式不正确！");
		} catch (ParamException e) {
			logger.error(e, e);
			return packaging(FAIL, "缺少必要参数！");
		}catch (Throwable t) {
			logger.error(t, t);
			return packaging(FAIL, "获取参数失败！");
		}

		Project project = new Project();
		project.setProjectId(projectId);
		
		projectNode.setEndTime(endTime);
		projectNode.setStartTime(startTime);
		projectNode.setProject(project);
		
		//只将resource保存到session就可以了，因为resource里面包含projectNode
		//保存到session
		//格式：
		//	"nodeNum0":projectNode
		//	"nodeNum1":projectNode
		//request.getSession().setAttribute("nodeNum" + nodeNum, projectNode);
		
		
		resource.setDownloadCount(0);
		resource.setKnowledgePoint(null);
		//projeceNode上传成功之后才上传resource
		resource.setProjectNode(projectNode);

		//保存到session
		request.getSession().setAttribute(Variables.resource + nodeNum, resource);
		
		//将两者返回，继续下一个节点的添加
		Map map = new HashMap<String,Object>();
		map.put(Variables.nodeNum, nodeNum);
		map.put(Variables.resource, projectId);
		return packaging(OK, map);
	}
	
	/**
	 * 完成项目的添加，保存到数据库中。将projectId和nodeNum发送到服务器,nodeNum应该是最后一次添加节点时的值
	 * {
	 * 	"projectId":"",
	 * 	"nodeNum":""
	 * }
	 * @return
	 */
	@POST
	@Path("/finish")
	public Response finishProject(String json,@Context HttpServletRequest request){
		
		//身份校验，只能教师上传项目
		logger.info("finishProject.json : " + json);
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		//从客户端获取projectId和nodeNum
		int projectId = 0;
		int nodeNum = 0;
		try {
			projectId = JsonHelper.getParam(json,Variables.projectId);
			nodeNum = JsonHelper.getParam(json,Variables.nodeNum);
		} catch (ParamFormateException e) {
			logger.error(e, e);
			return packaging(FAIL, "参数格式不正确！");
		} catch (ParamException e) {
			logger.error(e, e);
			return packaging(FAIL, "缺少必要参数！");
		}catch (Throwable t) {
			logger.error(t, t);
			return packaging(FAIL, "获取参数失败！");
		}
		
		//从session中获取resource
		for(int i = 0;true;i++){
			Resource resource = (Resource)request.getAttribute(Variables.resource + i);
			if(resource == null){
				//都添加完了
				if((i-1) < nodeNum){
					continue;
				}else{
					break;
				}
			}
			
			//从resource中获取projectNode，并添加到数据库
			ProjectNode projectNode = resource.getProjectNode();
			projectNode = projectNodeBaseDAO.create(projectNode);
			
			//将resource添加到数据库
			resource.setProjectNode(projectNode);
			resourceNodeBaseDAO.create(resource);
			
		}
		
		//将该project的状态设置为启用状态，单独的方法。
		Project project = updateProjectStatus(projectId,true);
		
		//发送一条系统通知，说明创建了一个新项目
		Notice notice = new Notice();
		notice.setClassId(project.getClasses());
		notice.setContent(login.getName() + "老师" + "新创建了一个项目！项目名称：" + project.getTitle());
		notice.setCreateTime(new Date());
		notice.setCreator(login);
		notice.setTitle(login.getName() + "老师" + "新创建了一个项目！");
		notice.setType(1);
		noticeBaseDAO.create(notice);
		return packaging(OK, Variables.project, project);
	}

	
	/**
	 * 调整项目的开启状态.status有两个值：0或者1。0表示关闭，1表示开启。
	 * {
	 * 	"status":"0"
	 * 	"projectId":"1"
	 * }
	 * @return
	 */
	@POST
	@Path("/open")
	public Response switchProjectStatus(String json,@Context HttpServletRequest request){
		//身份校验
		logger.info("finishProject.json : " + json);
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		//获取参数
		int status = -1;
		int projectId = 0;
		try {
			status = JsonHelper.getParam(json, "status");
			projectId = JsonHelper.getParam(json, Variables.projectId);
		} catch (ParamFormateException e) {
			logger.error(e, e);
		} catch (ParamException e) {
			logger.error(e, e);
		}catch (Exception e) {
			logger.error(e, e);
		}
		
		//校验参数
		boolean flag = false;
		if(status == 0 || status == 1){
			flag = (status == 1)?true:false;
		}else {
			return packaging(FAIL, "参数错误！");
		}
		
		//启用项目
		Project project = updateProjectStatus(projectId,flag);
		
		return (project == null)?packaging(FAIL, "更新失败，稍后重试！"):packaging(OK, Variables.project, project);
	}
	
	public Project updateProjectStatus(int projectId,boolean status){
		try{
			Project project = projectBaseDAO.lookup(Project.class, projectId);
			if (project == null) {
				throw new Exception();
			}
			project.setStatus(status);
			//修改项目的状态
			projectBaseDAO.update(project);
			return project;
		}catch(Throwable throwable){
			logger.error(throwable,throwable);
			return null;
		}
	}
	
	/**
	 * 删除项目
	 * @param json
	 * @return
	 */
	@POST
	@Path("/node/delete/{projectId}")
	public Response deleteProject(@PathParam(Variables.projectId) String projectId,@Context HttpServletRequest request){
		//身份校验
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		List<ProjectNode> nodes = projectNodeBaseDAO.list("from ProjectNode as pn where pn.project.projectId = " + Integer.valueOf(projectId));
		for(ProjectNode projectNode : nodes){
			deleteNode(projectNode);
		}
		
		//删除项目
		Project project = projectBaseDAO.lookup(Project.class, Integer.valueOf(projectId));
		projectBaseDAO.delete(project);
		
		//系统通知
		Notice notice = new Notice();
		notice.setClassId(project.getClasses());
		notice.setContent(login.getName() + "老师" + "删除了项目！项目名称：" + project.getTitle());
		notice.setCreateTime(new Date());
		notice.setCreator(login);
		notice.setTitle(login.getName() + "老师" + "删除了一个项目！");
		notice.setType(1);
		noticeBaseDAO.create(notice);
		
		return packaging(OK);
	}
	
	/**
	 * 删除节点
	 * @param projectNode
	 */
	public void deleteNode(ProjectNode projectNode){
		//将资源中的节点置空
		List<Resource> resources = resourceNodeBaseDAO.list("from Resource as r where r.projectNode.nodeId = " + projectNode.getNodeId());
		for(Resource resource : resources){
			resource.setProjectNode(null);
			resourceNodeBaseDAO.update(resource);
		}
		
		//将问题表中的节点置空
		List<Problem> problems = problemBaseDAO.list("from Problem as p where p.projectNode.nodeId = " + projectNode.getNodeId());
		for(Problem tmProblem : problems){
			tmProblem.setProjectNode(null);
			problemBaseDAO.update(tmProblem);
		}
		
		//将节点删除
		projectNodeBaseDAO.delete(projectNode);
	}
	
	/**
	 * 查询项目信息
	 * @param projectId
	 * @return
	 */
	@GET
	@Path("/{projectId}")
	public Response getProject(@PathParam("projectId") String projectId){
		
		int pid = StringHelper.isNumeric(projectId)?Integer.valueOf(projectId):0;
		if (pid == 0) {
			return packaging(FAIL, "projectId值错误!");
		}
		
		Project project = projectBaseDAO.lookup(Project.class, pid);
		return (project == null)?packaging(FAIL, "查询失败，请稍后重试！"):packaging(OK, Variables.project, project);
	}
	
	/**
	 * 学生专用。学生当前任务阶段。返回节点信息
	 */
	@GET
	@Path("/currentTask")
	public Response nowProjectNode(@Context HttpServletRequest request){
		
		User login = (User)request.getAttribute(USER);
		List<ProjectNode> projectNodes = projectNodeBaseDAO.list("from ProjectNode as pn where pn.project.Classes.classId = " + login.getClassId().getClassId() + " ORDER BY startTime asc");
		Date now = new Date();
		for(ProjectNode tmpProjectNode : projectNodes){
			if(!tmpProjectNode.getStartTime().before(now) || tmpProjectNode.getEndTime().after(now)){
				return packaging(OK, "projectNode", tmpProjectNode);
			}
		}
		//暂无任务
		return packaging(FAIL);
	}
	
	/**
	 * 得到资源或问题的节点的分类，其实是节点的标题
	 * @return
	 */
	@GET
	@Path("/nodeClassify")
	public Response getNodeClassify(){
		List<ProjectNode> nodes = projectNodeBaseDAO.list("from");
		List<String> names = new ArrayList<String>();
		for(ProjectNode projectNode : nodes){
			names.add(projectNode.getTitle());
		}
		return names.isEmpty()?packaging(FAIL,"分类为空！"):packaging(OK, "nodeClassify", names);
	}
	
}
