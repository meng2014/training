package cn.edu.cnu.training.service;

import static cn.edu.cnu.training.util.ResultHelper.packaging;
import static cn.edu.cnu.training.util.Variables.*;
import static cn.edu.cnu.training.vo.Results.*;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.edu.cnu.training.dao.BaseDAO;
import cn.edu.cnu.training.exception.ParamException;
import cn.edu.cnu.training.exception.ParamFormateException;
import cn.edu.cnu.training.pojo.Answer;
import cn.edu.cnu.training.pojo.Classes;
import cn.edu.cnu.training.pojo.KnowledgePoint;
import cn.edu.cnu.training.pojo.Problem;
import cn.edu.cnu.training.pojo.ProblemPush;
import cn.edu.cnu.training.pojo.ProjectNode;
import cn.edu.cnu.training.pojo.User;
import cn.edu.cnu.training.util.JsonHelper;
import cn.edu.cnu.training.util.StringHelper;
import cn.edu.cnu.training.util.Variables;

@Component
@Produces(produces)
public class ProblemService {
	
	private static Logger logger = LogManager.getLogger(ProblemService.class);
	
	@Autowired
	private BaseDAO<ProblemPush> problemPushBaseDAO;
	
	@Autowired
	private BaseDAO<Problem> problemBaseDAO;
	
	@Autowired
	private BaseDAO<Answer> answerBaseDAO;
	
	/**
	 * 将问题推送给班级
	 * {
	 * 	"problemId":"",
	 * 	"classId":""
	 * }
	 * @param json
	 * @param request
	 * @return
	 */
	@POST
	@Path("/push")
	public Response pushProblem(String json,@Context HttpServletRequest request){
		//身份校验。教师
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		//通过问题id获取问题
		int problemId = 0;
		int classId = 0;
		try {
			problemId = JsonHelper.getParam(json, Variables.problemId);
			classId = JsonHelper.getParam(json, Variables.classId);
		} catch (ParamFormateException e) {
			logger.error(e, e);
			return packaging(FAIL, "参数格式不对！");
		} catch (ParamException e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}
		
		
		//组装数据并保存到数据库
		Problem problem  = new Problem();
		problem.setProblemId(problemId);
		
		Classes classes = new Classes();
		classes.setClassId(classId);
		
		ProblemPush push = new ProblemPush();
		push.setProblem(problem);
		push.setPushTime(new Date());
		push.setReceiver(classes);
		push.setSender(login);
		
		push = problemPushBaseDAO.create(push);
		
		return packaging(OK, problemPush, push);
	}
	
	/**
	 * 用户新建问题
	 * @param json
	 * @param request
	 * @return
	 */
	@POST
	@Path("/add")
	public Response addProblem(String json,@Context HttpServletRequest request){
		
		//身份校验，教师和学生
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2 && login.getRole() != 1){
			return packaging(FAIL, "该用户暂无权限！");
		}

		//将数据组装
		Problem problem = JsonHelper.jsonToBean(json, Problem.class);
		if(problem == null){
			return packaging(FAIL, "数据异常！");
		}
		int knowledgePointId = 0;
		int projectNodeId = 0;
		try {
			knowledgePointId = JsonHelper.getParam(json, Variables.knowledgePointId);
			projectNodeId = JsonHelper.getParam(json, Variables.projectNodeId);
		} catch (ParamFormateException e) {
			logger.error(e, e);
			return packaging(FAIL, "参数格式不对！");
		} catch (ParamException e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}
		KnowledgePoint knowledgePoint = new KnowledgePoint();
		knowledgePoint.setKnowledgeId(knowledgePointId);
		ProjectNode projectNode = new ProjectNode();
		projectNode.setNodeId(projectNodeId);
		problem.setAnswerCount(0);
		problem.setCreateTime(new Date());
		problem.setCreator(login);
		problem.setKnowledgePoint(knowledgePoint);
		problem.setProjectNode(projectNode);
		problem.setTopAnswer(null);
		
		//添加到数据库
		problem = problemBaseDAO.create(problem);
		return packaging(OK, Variables.problem, problem);
	}
	
	/**
	 * 回答问题或回复评论。用type区别两者，0标识回答问题，1标识回复评论
	 * 回复评论，只允许二级回复。
	 * {
	 * 	"type":"0",
	 * 	"problemId":"",
	 * 	"commentedId":""
	 * }
	 * @param json
	 * @param request
	 * @return
	 */
	@POST
	@Path("/answer/add")
	public Response addAnswer(String json,@Context HttpServletRequest request){
		//身份校验，教师和学生
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2 && login.getRole() != 1){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		try {
			//判断回答问题还是回复评论
			int type = JsonHelper.getParam(json, "type");
			Answer answer = JsonHelper.jsonToBean(json, Answer.class);
			if (answer == null) {
				return packaging(FAIL, "参数不对");
			}
			answer.setAnswerTime(new Date());
			answer.setSpeaker(login);
			//回答问题：向问题表中添加
			if (type == 0) {
				int problemId = JsonHelper.getParam(json, Variables.problemId);
				Problem problem = new Problem();
				problem.setProblemId(problemId);
				answer.setProblem(problem);
				answer.setCommented(null);
			}
			
			//回复评论：想答案表中添加
			if (type == 1) {
				int commentedId = JsonHelper.getParam(json, "commentedId");
				Answer commented = new Answer();
				commented.setAnswerId(commentedId);
				answer.setCommented(commented);
				answer.setProblem(null);
			}
			
			answer = answerBaseDAO.create(answer);
			return (answer == null)?packaging(FAIL, "提供的参数不对！"):packaging(OK,Variables.answer, answer);
		} catch (ParamFormateException e) {
			logger.error(e, e);
			return packaging(FAIL, "参数格式不对！");
		} catch (ParamException e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}catch (Throwable e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}
	}
	
	/**
	 * 通过问题id查询问题及答案
	 * @return
	 */
	@GET
	@Path("/p/{problemId}")
	public Response getProblemById(@PathParam("problemId") String pId){
		
		int problemId = StringHelper.isNumeric(pId)?Integer.valueOf(pId):0;
		if (problemId == 0) {
			return packaging(FAIL, "参数不对！");
		}
		
		List<Answer> answers = answerBaseDAO.list("from Answer as a where a.problem.problemId = " + problemId);
		return (answers == null || answers.isEmpty())?packaging(FAIL, "没有该问题！"):packaging(OK, "answers", answers);
	}
	
	/**
	 * 正则匹配查询字段:关键字
	 */
	@POST
	@Path("/px/{str}")
	public Response getProblemByStr(@PathParam("str") String str){
		//hql语言中lower()方法可以解决英文中忽略大小写查询的问题
		List<Answer> answers = answerBaseDAO.list("from Answer as a where lower(a.content) like lower('%" + str +  "%')");
		return (answers == null || answers.isEmpty())?packaging(FAIL, "没有该问题！"):packaging(OK, "answers", answers);
	}
	
	
	/**
	 * {
	 * 	"problemId":"",
	 * 	"answerId":""
	 * }
	 * 答案置顶
	 */
	@POST
	@Path("/top")
	public Response topAnswer(String json,@Context HttpServletRequest request){
		//身份校验，教师和学生
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		//获取参数
		int problemId = 0;
		int answerId = 0;
		try {
			problemId = JsonHelper.getParam(json, Variables.problemId);
			answerId = JsonHelper.getParam(json, Variables.answerId);
		} catch (ParamFormateException e) {
			logger.error(e, e);
			return packaging(FAIL, "参数格式不对！");
		} catch (ParamException e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}catch (Throwable e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}
		
		Problem problem = problemBaseDAO.lookup(Problem.class, problemId);
		if (problem == null) {
			return packaging(FAIL, "问题不存在！");
		}
		Answer answer = answerBaseDAO.lookup(Answer.class, answerId);
		if (answer == null) {
			return packaging(FAIL, "答案不存在！");
		}
		problem.setTopAnswer(answer);
		
		return problemBaseDAO.update(problem)?packaging(OK):packaging(FAIL, "答案不存在，置顶失败！");
	}
	
	@GET
	@Path("/my")
	public Response myAnswer(@Context HttpServletRequest request){
		//身份校验，教师和学生
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 2){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		List<Answer> answers = answerBaseDAO.list("from Answer as a where a.speaker.id = " + login.getId());
		return (answers == null || answers.isEmpty())?packaging(FAIL, "暂时没有回答过问题"):packaging(OK, "answers", answers);
	}
}
