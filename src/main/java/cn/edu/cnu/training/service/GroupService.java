package cn.edu.cnu.training.service;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.edu.cnu.training.dao.GroupDao;
import cn.edu.cnu.training.pojo.User;
import cn.edu.cnu.training.util.StringHelper;
import static cn.edu.cnu.training.util.Variables.*;
import cn.edu.cnu.training.vo.Group;
import cn.edu.cnu.training.vo.Result;
import static cn.edu.cnu.training.vo.Results.*;
import static cn.edu.cnu.training.util.ResultHelper.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Component
@Produces(produces)
public class GroupService {
	
	@Autowired
	private GroupDao groupDao;
	/**
	 * request:成员列表
	 * {
	 * 	"members":12,32,14
	 * 	"headman":12(12为组长，如果不设，则表示默认第一个为组长)
	 * }
	 * @param group
	 * @return
	 */
	@POST
	@Path("/add")
	public Response addGroup(String members){
		JSONObject beanJson = JSONObject.fromObject(members);
		
		//获取members
		JSONArray member = beanJson.getJSONArray("members");
		if(member == null || member.size() == 0 || member.equals(null)){
			return packaging(FAIL,"成员不能为空！");
		}
		Set<Integer> memberSet = new HashSet<Integer>();
		for(int i = 0;i < member.size();i++){
			memberSet.add(member.getInt(i));
		}
		
		//获取组长
		String headman = beanJson.containsKey("headman")?beanJson.getString("headman"):(String)member.get(0);
		
		//添加到数据库
		boolean flag = groupDao.addGroup(memberSet, Integer.valueOf(headman));
		return flag?packaging(OK):packaging(FAIL,"添加失败，稍后重试！");
	}
	
	@POST
	@Path("/{groupId}/delete")
	public Response deleteGroup(@PathParam("groupId") String groupId){
		return groupDao.deleteGroup(groupId)?packaging(OK):packaging(FAIL, "删除失败，稍后重试！");
	}
	
	/**
	 * 查询群组
	 * @param groupId
	 * @return
	 */
	@GET
	@Path("/{groupId}")
	public Response getGroup(@PathParam("groupId") String groupId){
		Group group = groupDao.getGroup(groupId);
		
		return (group != null)?packaging(OK, "group", group):packaging(FAIL, "查无此群！");
	}
	
	/**
	 * 0表示关闭
	 * 1表示开启
	 * @param status
	 * @return
	 */
	@GET
	@Path("/open/{status}")
	public Response switchGroupStatus(@PathParam("status") String status,@Context HttpServletRequest request){
		User login = (User)request.getSession().getAttribute(USER);
		if (login == null || login.equals(null)) {
			return packaging(FAIL, "用户需要登录！");
		}
		boolean isNumeric = StringHelper.isNumeric(status);
		boolean isGroupOpen = false;
		if(isNumeric){
			if(Integer.valueOf(status) != 0 && Integer.valueOf(status) != 1){
				return packaging(FAIL, "参数错误！");
			}
			isGroupOpen = Integer.valueOf(status) == 1?true:false;
		}
		
		setGroupOpen(login, isGroupOpen);
		return packaging(OK);
	}
}
