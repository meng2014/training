package cn.edu.cnu.training.service;

import static cn.edu.cnu.training.util.Variables.USER;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.hibernate.property.IndexPropertyAccessor.IndexGetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.edu.cnu.training.dao.BaseDAO;
import cn.edu.cnu.training.pojo.Classes;
import cn.edu.cnu.training.pojo.Notice;
import cn.edu.cnu.training.pojo.User;
import cn.edu.cnu.training.util.ResultHelper;
import cn.edu.cnu.training.util.StringHelper;
import cn.edu.cnu.training.util.Variables;
import cn.edu.cnu.training.vo.Result;
import cn.edu.cnu.training.vo.Results;
import static cn.edu.cnu.training.util.ResultHelper.*;
import static cn.edu.cnu.training.vo.Results.*;
import net.sf.json.JSONObject;

/**
 * 管理所有的通知服务。包括系统通知和手动通知。
 * @author meng
 *
 */
@Component
@Produces(Variables.produces)
public class NoticeService {
	
	@Autowired
	private BaseDAO<Notice> noticeBaseDao;
	/**
	 * {
	 * 	"noticeId":"",
	 * 	"allNoticeByType":"",
	 * }
	 * allNoticeByType:0表示全部，1表示系统通知，2表示查看所有教师通知
	 * 如果有noticeId，则表示要查询单条信息。忽略allNoticeByType
	 * 
	 * @return
	 */
	@POST
	@Path("/get")
	public Response getNotice(String json,@Context HttpServletRequest request){
		User login = (User)request.getAttribute(Variables.USER);
		
		JSONObject jsonObject = JSONObject.fromObject(json);
		
		boolean hasNoticeId = jsonObject.containsKey("noticeId");
		boolean hasAllNoticeByType = jsonObject.containsKey("allNoticeByType");
		
		//如果有noticeId，则表示要查询单条信息。忽略allNoticeByType
		if (hasNoticeId) {
			//查询并返回notice对象
			Notice notice = (StringHelper.isNumeric(jsonObject.getString("noticeId")))?noticeBaseDao.lookup(Notice.class, jsonObject.getInt("noticeId")):null;
			return (notice == null)?packaging(FAIL, "查无数据！"):packaging(OK, "notice", notice);
		}
		
		//如果要查询allNoticeByType
		if(hasAllNoticeByType){
			String allNoticeByType = jsonObject.getString("allNoticeByType");
			
			//allNoticeByType是否在0,1,2范围内
			boolean isParmRight = StringHelper.isParmRight(allNoticeByType,0,1,2);
			if(isParmRight){
				//参数正确，直接查找。
				
				String hql = "";
				switch(Integer.valueOf(allNoticeByType)){
					case 0 :
							hql = "from Notice as n where n.classId.classId = " + login.getClassId().getClassId();
							break;
					case 1 : 
							hql = "from Notice as n where n.classId.classId = " + login.getClassId().getClassId() + " AND n.type = 1";
							break;
					case 2 : 
							hql = "from Notice as n where n.classId.classId = " + login.getClassId().getClassId() + " AND n.type = 2";
				}
				
				List<Notice> notices = noticeBaseDao.list(hql);
				
				return packaging(OK, "notices", notices);
			}
		}
		
		//参数错误
		return packaging(FAIL, "参数不正确！");
	}
	
	
	/**
	 * 请求数据：
	 * {
	 * 	"title":""
	 * 	"content":"",
	 * 	"classId":""
	 * }
	 * @param json
	 * @param request
	 * @return
	 */
	@POST
	@Path("/add")
	public Response addNotice(String json,@Context HttpServletRequest request){
		User login = (User)request.getAttribute(Variables.USER);
		if(login == null || login.getRole() < 2){
			return packaging(FAIL, "该用户没有权限！");
		}
		
		JSONObject jsonObject = JSONObject.fromObject(json);
		
		String title = "";
		String content = "";
		String classId = "";
		
		if(jsonObject.containsKey("content")){
			content = jsonObject.getString("content");
		}else{
			return packaging(FAIL, "通知内容不能为空！");
		}
		
		if(jsonObject.containsKey("classId")){
			classId = jsonObject.getString("classId");
			if (!StringHelper.isNumeric(classId)) {
				return packaging(FAIL, "班级id为非法字符！");
			}
		}else{
			return packaging(FAIL, "班级id不能为空！");
		}
		
		//如果没有携带title，则将content中前十个字符串加上“......”作为title，如果content的长度不够十，则取其全部。
		title = (jsonObject.containsKey("title"))?jsonObject.getString("title"):
				((content.length()>10)?content.substring(0, 9) + "......":content);
		
		Classes classes = new Classes();
		classes.setClassId(Integer.valueOf(classId));
		Notice notice = new Notice();
		notice.setClassId(classes);
		notice.setCreator(login);
		notice.setContent(content);
		notice.setCreateTime(new Date());
		notice.setTitle(title);
		notice.setType(2);
		
		return (noticeBaseDao.create(notice) != null)?packaging(OK):packaging(FAIL, "添加公告失败！");
	}
	
	
	@POST
	@Path("/delete/{noticeId}")
	public Response deleteNotice(@PathParam("noticeId") String noticeId,@Context HttpServletRequest request){
		User login = (User)request.getAttribute(Variables.USER);
		if(login == null || login.getRole() < 2){
			return packaging(FAIL, "该用户没有权限！");
		}
		
		//noticeId是数字，开始删除
		if(StringHelper.isNumeric(noticeId)){
			Notice notice = new Notice();
			notice.setNoticeId(Integer.valueOf(noticeId));
			return noticeBaseDao.delete(notice)?packaging(OK):packaging(FAIL, "删除失败！");
		}
		
		return packaging(FAIL, "noticeId值为非法参数！");
	}
	
}
