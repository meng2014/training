package cn.edu.cnu.training.service;


import static cn.edu.cnu.training.util.ResultHelper.packaging;
import static cn.edu.cnu.training.util.Variables.USER;
import static cn.edu.cnu.training.util.Variables.produces;
import static cn.edu.cnu.training.util.Variables.userId;
import static cn.edu.cnu.training.vo.Results.FAIL;
import static cn.edu.cnu.training.vo.Results.OK;

import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.mortbay.jetty.security.Credential.MD5;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.edu.cnu.training.dao.BaseDAO;
import cn.edu.cnu.training.exception.ParamException;
import cn.edu.cnu.training.exception.ParamFormateException;
import cn.edu.cnu.training.pojo.Classes;
import cn.edu.cnu.training.pojo.User;
import cn.edu.cnu.training.util.JsonHelper;
import cn.edu.cnu.training.util.MakeMd5;
import cn.edu.cnu.training.util.StringHelper;
import cn.edu.cnu.training.util.Variables;
import net.sf.json.JSONObject;
/**
 * 用户user服务
 * 其中：
 * 		@Path("/user"),选填。表示所有的url是/user的请求都交由此类处理，支持正则表达式。
 * 			注：用户提交的url被解析的过程分为几个部分：首先是匹配服务发布配置文件（restapp.xml）中address的值/userService,
 * 				然后匹配类路径即在类上配置的path，然后再匹配方法路径，即在方法上配置的路径。
 * 				例如restapp.xml中配置address是/userService，类中配置的路径是/file,方法中配置路径是/get/{userId}
 * 				，则用户访问的url应该是http://localhost:8080/training/userService/user/get/{userId}
 * 		@Produces({"application/json","application/xml"})表示可以返回的MIME类型
 * 		@Consumes注释代表的是一个资源可以接受的 MIME 类型
 * @author meng
 */
@Component
@Produces(produces)
public class UserService {
	private static Logger logger = LogManager.getLogger(UserService.class);
	@Autowired
	private BaseDAO<User> userBaseDao;
	@Autowired 
	private BaseDAO<Classes> classesBaseDAO;
	
	/**
	 * 查询单个特定子资源user。
	 * 其中@GET是子资源方法，也就是说配置了该项之后，java方法只接受http的get请求。其他的@POST,@PUT@DELETE都一样
	 * 		@Path("/get/{userId}")是子资源定位器，配置了此项之后，所有请求的url是：/userService/user/get/{userId}，都会执行此java方法
	 * 		写法等价于：@Path(value="/get/{userId}")
	 * @param userId 要查询的用户的id号
	 * @return
	 */
	@GET
	@Path("/{userId}")
	public Response getUser(@PathParam(userId) String userId,@Context HttpServletRequest request){
		
		logger.info("userId :  "  + userId);
		
		if(!StringHelper.isNumeric(userId)){
			return packaging(FAIL,"userId不能为包含字符");
		}
		User user = userBaseDao.lookup(User.class, Integer.valueOf(userId));
		return (user == null)?packaging(FAIL, "查无此人！"):packaging(OK,null,USER,user);
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@GET
	@Path("/all")
	public Response getAllUsers(@Context HttpServletRequest request){
		
		//授权
		User login = (User)request.getAttribute(USER);
		if (login == null || login.getRole() < 2) {
			return packaging(FAIL, "用户没有权限");
		}
		List<User> users = userBaseDao.list("from User");
		for(User tmp : users){
			tmp.setPassword("null");
		}
		return (users == null || users.isEmpty())?packaging(FAIL, "暂无用户信息！"):packaging(OK, null, USER, users);
	}
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@POST
	@Path("/login")
	public Response login(String userJson,@Context HttpServletRequest request){
		
		User user = JsonHelper.jsonToBean(userJson,User.class);
		User userValidate = validateUser(user);
		
		if(userValidate != null){
			//将用户的登录信息保存到客户端，只保存id和role字段，其他字段需要查询
			request.getSession().setAttribute(USER, getSessionUserInfo(userValidate));
			logger.info("user login!userId : " + userValidate.getId());
			return packaging(OK);
		}else{
			return packaging(FAIL);
		}
	}
	
	/**
	 * 将用户的信息过滤，将过滤后的用户信息保存到客户端session，目前只保存用户ID和role两个字段
	 * @param user
	 * @return
	 */
	private User getSessionUserInfo(User user) {
		user.setPassword("");
		return user;
	}

	/**
	 * 添加用户
	 * @param userJson
	 * @return
	 */
	@POST
	@Path("/add")
	public Response addUser(String userJson,@Context HttpServletRequest request){
		//授权
		User login = (User)request.getAttribute(USER);
		if (login == null || login.getRole() != 3) {
			return packaging(FAIL, "用户没有权限");
		}
		
		User user = JsonHelper.jsonToBean(userJson,User.class);
		if(user == null){
			return packaging(FAIL, "failed");
		}
		
		if (user.getRole() != 0 && user.getRole() != 1 && user.getRole() != 3) {
			return packaging(FAIL, "没有指定用户角色！");
		}
		
		user.setPassword(MakeMd5.toMd5(user.getPassword()));
		user = userBaseDao.create(user);
		logger.info("add user,userId : " + user.getId());
		return (user == null)?packaging(FAIL, "插入失败!"):packaging(OK);
	}
	
	/**
	 * 删除用户
	 * @param userId
	 * @return
	 */
	@POST
	@Path("/{userId}/delete")
	public Response deleteUser(@PathParam(userId) String userId){
		
		if(!StringHelper.isNumeric(userId)){
			return packaging(FAIL,"userId不能为包含字符",Variables.userId, userId);
		}
		
		boolean flag = userBaseDao.delete(new User(Integer.valueOf(userId)));
		logger.info("delete user , status : " + flag);
		return flag?packaging(OK):packaging(FAIL,"failed",Variables.userId, userId);
	}
	
	/**
	 * 更新用户信息
	 * @param userJson 用户json字符串
	 * @return
	 */
	@PUT
	@Path("/update")
	public Response updateUser(String userJson){
		
		User user = JsonHelper.jsonToBean(userJson,User.class);
		if(user == null){
			return packaging(FAIL, "用户信息出错!");
		}
		if(user.getId() < 1){
			return packaging(FAIL, "用户Id出错!");
		}
		
		User user2 = userBaseDao.lookup(User.class, user.getId());
		if (user2 == null) {
			return packaging(FAIL, "查无此用户！");
		}
		
		if (JSONObject.fromObject(userJson).containsKey("password")) {
			user.setPassword(MakeMd5.toMd5(user.getPassword()));
		}
		boolean flag = userBaseDao.update(user);
		user.setPassword("null");
		
		return flag?packaging(OK, null, USER, user):packaging(FAIL, "更新失败！");
	}
	
	/**
	 * 校验用户身份
	 * @param username
	 * @param password
	 * @return
	 */
	private User validateUser(String username,String password){
		if(username == null || password == null){
			return null;	
		}
		try {
			username = new String(username.getBytes("iso-8859-1"), "UTF-8");
			System.out.println(username);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String passwordMd5 = MakeMd5.toMd5(password);
		
		List<User> list = userBaseDao.list("from User as u where u.name='" + username +"' and u.password='" + passwordMd5 +"'");
		return list.size() == 0?null:list.get(0);
	}
	
	/**
	 * 校验用户身份。方法重载。
	 * @param user
	 * @return
	 */
	private User validateUser(User user){
		if(user == null){
			return null;
		}
		return validateUser(user.getName(),user.getPassword());
	}
	
	
	/**
	 * {
	 * 	"classId":"",
	 * 	"teacherId":""
	 * }
	 * @param json
	 * @param request
	 * @return
	 */
	@POST
	@Path("/relation")
	public Response relation(String json,@Context HttpServletRequest request){
		//身份校验，教师和学生
		User login = (User)request.getAttribute(USER);
		if(login.getRole() != 3){
			return packaging(FAIL, "该用户暂无权限！");
		}
		
		int classId = 0;
		int teacherId = 0;
		try {
			classId = JsonHelper.getParam(json, Variables.classId);
			teacherId = JsonHelper.getParam(json, "teacherId");
		} catch (ParamFormateException e) {
			logger.error(e, e);
			return packaging(FAIL, "参数格式不对！");
		} catch (ParamException e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}catch (Throwable e) {
			logger.error(e, e);
			return packaging(FAIL,"参数不存在！");
		}
		
		Classes classes = classesBaseDAO.lookup(Classes.class, classId);
		if (classes == null) {
			return packaging(FAIL, "查无此班！");
		}
		User teacher = userBaseDao.lookup(User.class, teacherId);
		if (teacher == null) {
			return packaging(FAIL, "查无此教师！");
		}
		classes.setTeacher(teacher);
		
		return classesBaseDAO.update(classes)?packaging(OK):packaging(FAIL, "查无此教师！");
	}
}
