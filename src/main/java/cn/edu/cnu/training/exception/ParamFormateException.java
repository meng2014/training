package cn.edu.cnu.training.exception;
/**
 * 在获取json时，参数获取失败。
 * 一般为json中有该字段，但格式不对
 * @author meng
 *
 */
public class ParamFormateException extends ParamException {
}
