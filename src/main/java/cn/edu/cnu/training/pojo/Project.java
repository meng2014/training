package cn.edu.cnu.training.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.sf.json.JSONObject;
@Entity
@Table(name="project")
public class Project {
	private int projectId;
	private String title;
	private User creator;
	private Date createTime;
	private String content;
	private Date startTime;
	private Boolean status;
	private Date publishTime;
	private Date endTime;
	private Classes Classes;
	
	
	@Override
	public String toString() {
		return JSONObject.fromObject(this).toString();
	}
	
	@Id
	@GeneratedValue
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="creatorId")
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Date getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(Date publishTime) {
		this.publishTime = publishTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="classId")
	public Classes getClasses() {
		return Classes;
	}

	public void setClasses(Classes classes) {
		Classes = classes;
	}
	
}
