package cn.edu.cnu.training.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="user")
public class User {
	/**
	 * 学生所在班级信息
	 */
	private Classes classId;
	private Date createTime;
	/**
	 * 学生所属组别。UUID
	 */
	private String groupId;
	private int id;
	/**
	 * 是否组长
	 */
	private Boolean isHeadman;
	private Date modifyTime;
	private String name;
	private String password;
	/**
	 * 1表示学生，2表示教师，3表示管理员，其他非系统用户
	 */
	private int role;
	
	public User(){}
	public User(int id){
		this.id = id;
	}
	/**
	 * <li>@mappedBy是一对多的“一方”设置的标识一对多关系由“多方”进行维护的标识。</li>
	 * @return
	 */
//	@OneToMany(mappedBy="teacher",cascade={CascadeType.ALL})
//	public Set<Classes> getClasses() {
//		return classes;
//	}

	/**
	 * <li>@JoinColumn是设置一个外键关联。属性name里面的值是本表的列名和外键的名字。是数据库视图。</li>
	 * @return
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="classId")
	public Classes getClassId() {
		return classId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public String getGroupId() {
		return groupId;
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public Boolean getIsHeadman() {
		return isHeadman;
	}
	
	
	public Date getModifyTime() {
		return modifyTime;
	}
	public String getName() {
		return name;
	}
	
	public String getPassword() {
		return password;
	}
	public int getRole() {
		return role;
	}
	public void setClassId(Classes classId) {
		this.classId = classId;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setIsHeadman(Boolean isHeadman) {
		this.isHeadman = isHeadman;
	}
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setRole(int role) {
		this.role = role;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "id : " + id + "\nname : " + name + "\npassword : " + password;
	}
}
