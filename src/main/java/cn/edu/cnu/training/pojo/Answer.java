package cn.edu.cnu.training.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="answer")
public class Answer {
	private int answerId;
	private User speaker;
	private Date answerTime;
	private Problem problem;
	private String content;
	private Answer commented;
	
	@Id
	@GeneratedValue
	public int getAnswerId() {
		return answerId;
	}
	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="speakerId")
	public User getSpeaker() {
		return speaker;
	}
	public void setSpeaker(User speaker) {
		this.speaker = speaker;
	}
	public Date getAnswerTime() {
		return answerTime;
	}
	public void setAnswerTime(Date answerTime) {
		this.answerTime = answerTime;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="problemId")
	public Problem getProblem() {
		return problem;
	}
	public void setProblem(Problem problem) {
		this.problem = problem;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="commentedId")
	public Answer getCommented() {
		return commented;
	}
	public void setCommented(Answer commented) {
		this.commented = commented;
	}
	
	
}
