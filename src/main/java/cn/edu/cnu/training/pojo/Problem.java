package cn.edu.cnu.training.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="problem")
public class Problem {
	private int problemId;
	private String title;
	private String content;
	private User creator;
	private Date createTime;
	private ProjectNode projectNode;
	private KnowledgePoint knowledgePoint;
	private Answer topAnswer;
	private int AnswerCount;
	
	@Id
	@GeneratedValue
	public int getProblemId() {
		return problemId;
	}
	public void setProblemId(int problemId) {
		this.problemId = problemId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="creatorId")
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="projectNodeId")
	public ProjectNode getProjectNode() {
		return projectNode;
	}
	public void setProjectNode(ProjectNode projectNode) {
		this.projectNode = projectNode;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="knowledgePointId")
	public KnowledgePoint getKnowledgePoint() {
		return knowledgePoint;
	}
	public void setKnowledgePoint(KnowledgePoint knowledgePoint) {
		this.knowledgePoint = knowledgePoint;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="topAnswerId")
	public Answer getTopAnswer() {
		return topAnswer;
	}
	public void setTopAnswer(Answer topAnswer) {
		this.topAnswer = topAnswer;
	}
	public int getAnswerCount() {
		return AnswerCount;
	}
	public void setAnswerCount(int answerCount) {
		AnswerCount = answerCount;
	}
	
}
