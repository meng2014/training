package cn.edu.cnu.training.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="notice")
public class Notice {
	private Classes classId;
	private String content;
	private Date createTime;
	private User creator;
	private int noticeId;
	private String title;
	private int type;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="classId")
	public Classes getClassId() {
		return classId;
	}
	public String getContent() {
		return content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="creatorId")
	public User getCreator() {
		return creator;
	}
	@Id
	@GeneratedValue
	public int getNoticeId() {
		return noticeId;
	}
	public String getTitle() {
		return title;
	}
	
	public void setClassId(Classes classId) {
		this.classId = classId;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public void setNoticeId(int noticeId) {
		this.noticeId = noticeId;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
}
