package cn.edu.cnu.training.pojo;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 班级信息
 * @author meng
 *
 */
@Entity
@Table(name="classes")
public class Classes {
	private int classId;
	/**
	 * 入学年份：2014,2015等
	 */
	private String gradeYear;
	/**
	 * 专业名：计算机教育，软件工程等
	 */
	private String major;
	/**
	 * 教师uid。表示教师正在教授此班
	 */
	private User teacher;
	@Id
	@GeneratedValue
	public int getClassId() {
		return classId;
	}
	public String getGradeYear() {
		return gradeYear;
	}
	public String getMajor() {
		return major;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="teacherId")
	public User getTeacher() {
		return teacher;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public void setGradeYear(String gradeYear) {
		this.gradeYear = gradeYear;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	
	public void setTeacher(User teacher) {
		this.teacher = teacher;
	}
	
}
