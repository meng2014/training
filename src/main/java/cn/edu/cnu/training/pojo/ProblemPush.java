package cn.edu.cnu.training.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="problemPush")
public class ProblemPush {
	private int pushId;
	private Problem problem;
	private User sender;
	private Classes receiver;
	private Date pushTime;
	
	@Id
	@GeneratedValue
	public int getPushId() {
		return pushId;
	}
	public void setPushId(int pushId) {
		this.pushId = pushId;
	}
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="problemId")
	public Problem getProblem() {
		return problem;
	}
	public void setProblem(Problem problem) {
		this.problem = problem;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="senderId")
	public User getSender() {
		return sender;
	}
	public void setSender(User sender) {
		this.sender = sender;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="classId")
	public Classes getReceiver() {
		return receiver;
	}
	public void setReceiver(Classes receiver) {
		this.receiver = receiver;
	}
	public Date getPushTime() {
		return pushTime;
	}
	public void setPushTime(Date pushTime) {
		this.pushTime = pushTime;
	}
	
}
