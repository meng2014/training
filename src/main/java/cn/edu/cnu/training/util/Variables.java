package cn.edu.cnu.training.util;

import javax.ws.rs.Produces;

import cn.edu.cnu.training.pojo.User;

public class Variables {
	private static boolean isGroupOpen = false;
	public static final String USER = "user";
	public static final String userId = "userId";
	public static final String produces = "application/json; charset=UTF-8";
	public static final String project = "project";
	public static final String projectId = "projectId";
	public static final String classId = "classId";
	public static final String nodeNum = "nodeNum";
	public static final String startTime = "startTime";
	public static final String endTime = "endTime";
	public static final String resource = "resource";
	public static final String problemId = "problemId";
	public static final String problemPush = "problemPush";
	public static final String knowledgePointId = "knowledgePointId";
	public static final String projectNodeId = "projectNodeId";
	public static final String problem = "problem";
	public static final String answer = "answer";
	public static final String answerId = "answerId";
	
	public static boolean isGroupOpen() {
		return isGroupOpen;
	}

	public static boolean setGroupOpen(User user,boolean isGroupOpen) {
		//教师和管理员才可以修改群组功能的开关
		if(user.getRole()>1){
			Variables.isGroupOpen = isGroupOpen;
			return true;
		}
		return false;
	}
	
}
