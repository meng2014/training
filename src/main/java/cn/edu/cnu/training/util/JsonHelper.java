package cn.edu.cnu.training.util;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import cn.edu.cnu.training.exception.ParamException;
import cn.edu.cnu.training.exception.ParamFormateException;
import cn.edu.cnu.training.pojo.Project;
import net.sf.json.JSONObject;

public class JsonHelper {
	private static Logger logger = LogManager.getLogger(JsonHelper.class);
	public static <T> T jsonToBean(String json,Class<T> c){
		try{
			JSONObject beanJson = JSONObject.fromObject(json);
			T t = (T)JSONObject.toBean(beanJson, c);
			return t;
		}catch(Throwable t){
			//转换失败，格式不对
			logger.error(t,t);
			return null;
		}
	}
	
	/**
	 * 获取一个json串中特定字段，并转换成int，如果转换错误抛异常。
	 * @return
	 */
	public static int getParam(String json,String key) throws ParamException{
		JSONObject jsonObject = JSONObject.fromObject(json);
		int param = -30000;
		
		if (jsonObject.containsKey(key)) {
			param = StringHelper.isNumeric(jsonObject.getString(key))?jsonObject.getInt(key):-30000;
			if (param == -30000) {
				throw new ParamFormateException();
			}
		}else{
			throw new ParamException();
		}
		return param;
	}
	
	
	/**
	 * 获取给定json串中日期格式的字段，如果失败，则抛异常
	 * @param json
	 * @param key
	 * @return
	 * @throws ParamException
	 */
	public static Date getDateParam(String json,String key) throws ParamException{
		JSONObject jsonObject = JSONObject.fromObject(json);
		Date param = null;
		
		if (jsonObject.containsKey(key)) {
			param = isDate(jsonObject.getString(key))?getDate(json,key):null;
			if (param == null) {
				throw new ParamFormateException();
			}
		}else{
			throw new ParamException();
		}
		return param;
	}
	
	
	/**
	 * 从json中获取日期字段
	 * @param json
	 * @param key
	 * @return
	 */
	private static Date getDate(String json, String key) {
		JSONObject jsonObject = JSONObject.fromObject(json);
		String dateTmp = jsonObject.getString(key);
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd hh:mm:ss");
		if (dateTmp != null && !dateTmp.equals("")) {
			if (dateTmp.split("/").length > 1) {
				dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
			}
			if (dateTmp.split("-").length > 1) {
				dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			}
			if (dateTmp.split(".").length > 1) {
				dateFormat = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
			}
			
		} else {
			return null;
		}
		Date d = null;
		try {
			dateTmp = isOnlyDate(dateTmp)?dateTmp + " 00:00:00":dateTmp;
			d = dateFormat.parse(dateTmp);
		} catch (ParseException e) {
			logger.error(e, e);
		}
		return d;
	}
	
	/**
	 * 校验日期。
	 * 2015-12-13
	 *	2015/02/15
	 *	2015.2.1
	 *-./都可以
	 */
	public static boolean isOnlyDate(String string){
		
		String tString = "[0-9]{4}[-./][0-1]?[0-9][-./][0-3]?[0-9]";
		Pattern p = Pattern.compile(tString);
		Matcher m = p.matcher(string);
		boolean b = m.matches();
		return b;
	}
	
	/**
	 * 校验完整日期
	 * @param string
	 * @return
	 */
	public static boolean isDate(String string){
		String tString = "[0-9]{4}[-./][0-1]?[0-9][-./][0-3]?[0-9] ?[0-2]?[0-9]?:?[0-5]?[0-9]?:?[0-5]?[0-9]?";
		Pattern p = Pattern.compile(tString);
		Matcher m = p.matcher(string);
		boolean b = m.matches();
		return b;
	}
	
}
