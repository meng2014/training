package cn.edu.cnu.training.util;

import static cn.edu.cnu.training.util.Variables.USER;

import java.util.Map;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import cn.edu.cnu.training.vo.Result;
import cn.edu.cnu.training.vo.Results;
import net.sf.json.JSONObject;

public class ResultHelper {
	
	private static ResponseBuilder builder = Response.ok();
	private static Result result = new Result();
	
	public static String getFailedResult(Result result,String failedMsg){
		return JSONObject.fromObject(result.setStatus(Results.FAIL).setMsg(failedMsg)).toString();
	}
	
	/**
	 * 将要返回到客户端的信息打包并返回。
	 * @param status 执行成功或失败的状态
	 * @param msg 提示信息
	 * @param key 返回值key
	 * @param value 返回值value
	 * @return
	 */
	public static Response packaging(String status,String msg,String key,Object value){
		return builder.entity(JSONObject.fromObject(result.setStatus(status).setMsg(msg).setData(key, value)).toString()).build();
	}
	
	public static Response packaging(String status,String key,Object value){
		return builder.entity(JSONObject.fromObject(result.setStatus(status).setData(key, value)).toString()).build();
	}
	
	public static Response packaging(String status){
		return builder.entity(JSONObject.fromObject(result.setStatus(status)).toString()).build();
	}
	
	public static Response packaging(String status,Map<String,Object> map){
		result.getResults().getDatas().putAll(map);
		return packaging(status);
	}
	
	public static Response packaging(String status,String msg){
		return builder.entity(JSONObject.fromObject(result.setStatus(status).setMsg(msg)).toString()).build();
	}
}
