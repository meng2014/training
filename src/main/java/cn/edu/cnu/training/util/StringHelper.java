package cn.edu.cnu.training.util;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class StringHelper {

	/**
	 * 验证一个字符串是否一个正整数
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		if (str == null) {
			return false;
		}
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}
	
	/**
	 * 判断字符所代表的数字，是否属于某一个范围
	 */
	public static boolean isParmRight(String str,int... values){
		
		if(values.length == 0){
			return false;
		}
		
		//先检查是否为数值型
		boolean isNum = isNumeric(str);
		
		if(!isNum){
			return false;
		}
		
		//检查是否在给定的范围
		int strNum = Integer.valueOf(str);
		for(int tmp : values){
			if(strNum == tmp){
				return true;
			}
		}
		
		return false;
	}
	
}
